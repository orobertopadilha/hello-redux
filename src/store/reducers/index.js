const initialState = { number: 0 }

const numberReducer = (state = initialState, action) => {

    switch (action.type) {
        case 'generate-number': 
            state = { number: action.payload }
            return state
        case 'clear-number':
            state = initialState
            return state;
        default: 
            return state;
    }
}

export default numberReducer