export const generateNumber = (newNumber) => {
    return {
        type: 'generate-number',
        payload: newNumber
    }
}

export const clearNumber = () => {
    return { 
        type: 'clear-number'
    }
}