import { useDispatch, useSelector } from 'react-redux'
import { generateNumber, clearNumber } from './store/actions'

const App = () => {
    const number = useSelector(state => state.number)
    const dispatch = useDispatch()

    const generate = () => {
        let newNumber = Math.floor(Math.random() * 1000)
        dispatch(generateNumber(newNumber))
    }

    const clear = () => {
        dispatch(clearNumber())
    }

    return (
        <>
            <h1>Hello, Redux!</h1>

            Último número gerado: {number} <br /><br />
            <button onClick={generate}>Gerar novo número</button>
            <button onClick={clear}>Limpar</button>
        </>
    )
}

export default App